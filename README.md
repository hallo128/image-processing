# image-processing

## 介绍
深度学习的图像处理
- 深度学习的图像处理，按不同网络模型建立文件夹

## 文件说明
```
├── data_set     数据集下载、文件夹划分
├── image_classfication     图像分类
  ├── custom_dataset        0.图像数据处理
  ├── train_multi_GPU       1.GPU运行(单机/多机，单机时含完整的训练模型流程介绍)
```

## 所需环境
* d2l（建议使用）
* python3.6/3.7/3.8/3.9
* vscode/pycharm (IDE)
* pytorch 1.12 (pip package)
* torchvision 0.13.0 (pip package)
* wandb 0.16.2 (pip package)

包安装：
```pip install -r requirements.txt```

### 环境安装
参考链接：https://zh.d2l.ai/chapter_installation/index.html

d2l环境已经包含深度学习的大部分包，就不用一一安装
```
# 1.先安装Miniconda(见参考上述链接，或者https://docs.conda.io/projects/miniconda/en/latest/)

mkdir -p ~/miniconda3
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/miniconda3/miniconda.sh
bash ~/miniconda3/miniconda.sh -b -u -p ~/miniconda3
rm -rf ~/miniconda3/miniconda.s

~/miniconda3/bin/conda init bash
~/miniconda3/bin/conda init zsh

# 2.创建和激活环境
conda create --name d2l python=3.9 -y
conda activate d2l

# 3.安装深度学习所需包
pip install torch==1.12.0
pip install torchvision==0.13.0
pip install d2l==0.17.6
```

### wandb监控使用

- 参考资料推荐：https://blog.csdn.net/qq_40507857/article/details/112791111

- 4项核心功能：
    - 看板：跟踪训练过程，给出可视化结果
    - 报告：保存和共享训练过程中一些细节、有价值的信息
    - 调优：使用超参数调优来优化你训练的模型
    - 工具：数据集和模型版本化

- 强大的兼容性：能够和Jupyter、TensorFlow、Pytorch、Keras、Scikit、fast.ai、LightGBM、XGBoost一起结合使用

- 基本接口
  - wandb.init — 在训练脚本开头初始化一个新的运行项；
  - wandb.config — 跟踪超参数；
  - wandb.log — 在训练循环中持续记录变化的指标；
  - wandb.save — 保存运行项相关文件，如模型权值；
  - wandb.restore — 运行指定运行项时，恢复代码状态。

#### 使用前的注册和登录
1. 注册官方网址: https://wandb.ai （推荐使用微软账号注册）
2. 安装 wandb：```pip install wandb```
3. 登录 wandb：在命令行中运行 ```wandb login```
4. 按提示复制粘贴API Key至命令行中（查看网址：https://wandb.ai/authorize）【提示：务必先在网页注册账号】

#### python使用代码

1. 初始化：不同项目project，在每次name（不同运行时间）下的结果（月-日-时-分-秒）

可以声明超参数:
- wandb.config.dropout = 0.2
- wandb.config.hidden_layer_size = 128
2. 运行训练：代码内增加监控。监控内容：字典类型，如:
- log_test = {}
- log_test['epoch'] = epoch
3. 保存文件
```wandb.save("mymodel.h5")```
或者
```model.save(os.path.join(wandb.run.dir, "mymodel.h5"))```

```
import wandb

# 1.初始化：项目fruit30，当前（月-日-时-分-秒）
wandb.init(project='fruit30', name=time.strftime('%m%d%H%M%S'))

# 2.运行训练：代码内增加监控
wandb.log(log_test)  #log_test为字典

# 3.将字典同时保存到本地文件
# (1)字典 -> 数据框
df_test_log = pd.DataFrame()
df_test_log = df_test_log.append(log_test, ignore_index=True)

# (2)数据框 -> csv
df_test_log.to_csv('训练日志-测试集.csv', index=False)
```

说明：Ctrl+C结束训练进程出现“wandb: / 0.124 MB of 0.124 MB uploaded (0.000 MB deduped)“解决方法，如下运行：
```ps aux|grep wandb|grep -v grep | awk '{print $2}'|xargs kill -9```


#### 软件架构
软件架构说明
使用Pytorch进行网络的搭建与训练 

## 0. data_set 图像分类
### 主要功能
1. 数据集下载存放
2. 划分文件夹：训练集和测试集（非必要）

### 终端运行：数据下载并划分
```
chmod +x download_flower_data.sh
./download_flower_data.sh
python split_data.py
```




## image_classfication 图像分类
1.  custom_dataset
2.  xxxx
3.  xxxx


### 1. custom_dataset 
#### 主要功能
DataSet和DataLoader数据集构建及使用
#### 主要文件


### 2.  

#### 使用说明
1.  图像分类算法
  * LeNet

2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

