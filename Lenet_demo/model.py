import torch.nn as nn
import torch.nn.functional as F

# 自定义模型类：LeNet
class LeNet(nn.Module):
    # 继承初始化函数
    def __init__(self):
        super(LeNet, self).__init__()
        # 搭建过程中的网络层结构
        self.conv1 = nn.Conv2d(3, 16, 5)  #卷积层
        self.pool1 = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(16, 32, 5)
        self.pool2 = nn.MaxPool2d(2, 2)
        self.fc1 = nn.Linear(32*5*5, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 10)


    # 正向传播过程
    def forward(self, x):
        #x：输入数据，格式为tensor【batch, channel, height, width】
    
        x = F.relu(self.conv1(x))    # input(3, 32, 32) output(16, 28, 28)
        x = self.pool1(x)            # output(16, 14, 14)
        x = F.relu(self.conv2(x))    # output(32, 10, 10)
        x = self.pool2(x)            # output(32, 5, 5)
        x = x.view(-1, 32*5*5)       # output(32*5*5)   数据展平: view中一个参数定为-1，代表动态调整这个维度上的元素个数
        x = F.relu(self.fc1(x))      # output(120)
        x = F.relu(self.fc2(x))      # output(84)
        x = self.fc3(x)              # output(10)
        return x

'''
#-----模型测试-----
import torch
input1 = torch.rand([32, 3, 32,32])

#模型
model = LeNet()
print(model)   #1. 打印模型结构

#2.打断点，查看shape的变化
output = model(input1)
'''