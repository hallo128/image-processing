from PIL import Image
import torch
from torch.utils.data import Dataset


class MyDataSet(Dataset):
    """自定义数据集"""

    def __init__(self, images_path: list, images_class: list, transform=None):
        # images_path: 图片路径
        # images_class: 图片标签
        # transform: 图片预处理方式
        self.images_path = images_path
        self.images_class = images_class
        self.transform = transform

    def __len__(self):
        return len(self.images_path)

    def __getitem__(self, item):
        img = Image.open(self.images_path[item])  #默认类型都是PIL，默认带的预处理方法
        # RGB为彩色图片，L为灰度图片
        if img.mode != 'RGB':    # ----目前仅处理彩色图片，其他图像格式，【可设断点】，查看img信息(排查图片信息)
            raise ValueError("image: {} isn't RGB mode.".format(self.images_path[item]))
        label = self.images_class[item]

        if self.transform is not None:
            img = self.transform(img)

        return img, label

    @staticmethod
    def collate_fn(batch):
        '''
        静态方法
        输入：
            batch: (img, label)元组
        '''
        # 官方实现的default_collate可以参考
        # https://github.com/pytorch/pytorch/blob/67b7e751e6b5931a9f45274653f4f653a4e6cdf6/torch/utils/data/_utils/collate.py
        images, labels = tuple(zip(*batch))

        images = torch.stack(images, dim=0)  # 在0纬度进行拼接 [8, 3,224,224]
        labels = torch.as_tensor(labels)     # 转换为tensor格式：8个标签
        return images, labels

