## 主要功能
1. 数据保存为DataSet格式
2. 数据保存为DataLoader格式
3. 绘制一组DataLoader的小批量图片

## 文件说明
```
核心
├── test_my_dataset.py      测试生成DataSet和DataLoar的批量效果
├── my_dataset.py           自定义DataSet数据格式
├── utils.py                文件夹读取和划分，绘制批量数据
功能
├── class_indices.json      自定义文件夹生成的标签值和索引
├── train_CIFAR10.py        对CIFAR10数据集进行训练
```
启动train_CIFAR10训练：
```
1. 先下载预训练权重并重命名
wget https://download.pytorch.org/models/resnet34-333f7ec4.pth

mv resnet34-333f7ec4.pth resNet34.pth

2. 冻结最后一层，并修改epochs轮数
python train_CIFAR10.py --freeze-layers True --epochs 30 
```


## 1.数据保存为DataSet格式
### 法一：全部数据存放在一个文件夹内，按类别分好
优势：
1. 能生成标签索引对应的json文件：class_indices.json【索引 -> 标签名称】
2. 频数统计：绘制每个标签的条形图
3. 自定义数据处理方式
```
1. utils.read_split_data 函数：json文件；划分训练集、验证集为列表；频数统计
2. my_dataset.MyDataSet 函数：自定义数据集

```

### 法二：事先把训练和验证数据放在不同文件夹中
```
1. 划分训练和验证数据放在不同文件夹中【具体查看data_set中的split_data.py文件】
2. datasets.ImageFolder
```

### 法三：使用datasets中内置的公共数据集
具体查看：https://pytorch.org/vision/stable/datasets.html

## 测试文件调试
文件：test_my_dataset.py
### 主要功能测试：
1. 上述3个方法的使用
2. 绘图查看小批量
3. utils.read_split_data 函数测试

#### utils.read_split_data 函数测试
1. 能生成标签索引对应的json文件：class_indices.json【索引 -> 标签名称】
```
{
    "0": "daisy",
    "1": "dandelion",
    "2": "roses",
    "3": "sunflowers",
    "4": "tulips"
}
```

2. 绘制每个类别的个数条形图
![class_names.jpg](class_names.jpg "每个类别的个数条形图")
- 可修改：plot_image = True
- 是否绘制【每个类别的个数条形图】，修改为True，在return设置断点，在test_my_dataset.py进行运行调试

centos中图像查看：
- 法一：将图片保存下来查看
- 法二：在运行时，选择"交互窗口中运行当前文件"

#### utils.plot_data_loader_image 函数
绘制一个batch的读取数据，查看原始数据

centos中图像查看：
- 法一：将图片保存下来查看  "batch_data_loader_image.jpg"
![batch_data_loader_image.jpg](batch_data_loader_image.jpg "一个batch图像")
- 法二：在运行时，选择"交互窗口中运行当前文件"

download_datasets_cifar10.py：从官网下载指定数据集（需要3个小时）
后台不中断下载（3个小时左右下载完成）运行代码：
```
nohup python download_datasets_cifar10.py &
或者
nohup python download_datasets_cifar10.py > out.log &
```
