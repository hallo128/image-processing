import os

import torch
from torchvision import transforms

from torchvision import datasets
from my_dataset import MyDataSet
from utils import read_split_data, plot_data_loader_image

# https://storage.googleapis.com/download.tensorflow.org/example_images/flower_photos.tgz
# 数据集所在根目录
root = "/home/lighthouse/gitee/image-processing/data_set/flower_data"   #---
# 完整数据文件夹【法一】
full_data_path = os.path.join(root, 'flower_photos')        #---
# 训练数据文件夹【法二】
train_path = os.path.join(root, 'train')
# 验证数据文件夹【法二】
train_path = os.path.join(root, 'val')


def main():
    # 1. 设备
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    print("using {} device.".format(device))

    # 2. 定义训练集和测试集的图像预处理方法
    data_transform = {
        "train": transforms.Compose([transforms.RandomResizedCrop(224),  # 随机裁剪
                                     transforms.RandomHorizontalFlip(),  # 随机水平翻转
                                     transforms.ToTensor(),
                                     transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])]),
        "val": transforms.Compose([transforms.Resize(256),
                                   transforms.CenterCrop(224),
                                   transforms.ToTensor(),
                                   transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])])}

    # 3. 数据保存为DataSet格式
    # 训练集
    # 法一：全部数据存放在一个文件夹内，按类别分好
    # (1) 划分训练集和测试集
    # train_images_path       # 列表：存储训练集的所有图片【路径】
    # train_images_label      # 列表：存储训练集图片对应【类别索引】信息
    # val_images_path         # 列表：存储验证集的所有图片【路径】
    # val_images_label        # 列表：存储验证集图片对应【类别索引】信息
    train_info, val_info, num_classes = read_split_data(full_data_path)
    train_images_path, train_images_label = train_info
    val_images_path, val_images_label = val_info
    
    # (2) 自定义数据格式
    train_data_set = MyDataSet(images_path=train_images_path,      # 训练集所有路径
                               images_class=train_images_label,    # 训练集所有标签
                               transform=data_transform["train"])  # 训练集预处理方法
    
    # 法二：文件夹数据：需要将训练数据集单独存放到一个文件夹中
    # 事先把训练和测试数据放在不同文件夹中【具体查看data_set中的split_data.py文件】
    # train_data_set = datasets.ImageFolder(train_path,   # train训练集文件夹的根目录
    #                                       data_transform["train"])

    # 法三：使用datasets中内置的公共数据集
    # 训练集
    # train_data_set = datasets.CIFAR10(
    #     root="../../data_set",   # 指定数据存放路径
    #     train=True,              # 训练集
    #     transform= data_transform["train"], 
    #     download=True)        # True:没有下载过，会先下载数据
    # 测试集
    # test_data = datasets.CIFAR10(root='../data', 
    #                              train=False,  # 测试集
    #                              transform = data_transform["val"],
    #                              download=True)

    # 4. 数据保存为DataLoader格式
    batch_size = 8
    nw = min([os.cpu_count(), batch_size if batch_size > 1 else 0, 8])  # number of workers
    # 工作进程的上限为8
    print('Using {} dataloader workers'.format(nw))
    # 训练集
    train_loader = torch.utils.data.DataLoader(train_data_set,
                                               batch_size=batch_size,
                                               shuffle=True,    # 随机打乱
                                               num_workers=nw  # 注：测试的时候，可以设置为0
                                               # 只在需要自己定义打包元素方法的时候，才需要自定义
                                               # collate_fn=train_data_set.collate_fn
                                               )
    # # 测试一小批数据
    # for data in train_loader:
    #     images, labels = data
    #     print(images.shape)
    #     print(labels)

    #     break
    # 5. 绘制一组DataLoader的小批量图片
    plot_data_loader_image(train_loader)

    # 重写一个绘制小批量图片的方法，不能依赖json文件 / 如何知道dataset中标签的索引对应值

    # for step, data in enumerate(train_loader):
    #     images, labels = data


if __name__ == '__main__':
    main()
