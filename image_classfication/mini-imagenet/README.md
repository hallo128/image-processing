# 读取mini-imagenet数据

### 1.数据存放文件夹 mini_imagenet
包含的文件：
```
├── mini_imagenet: 数据集根目录【root_dir: 自己的存放路径，如：/root/.advertorch/mini_imagenet】
     ├── images: 所有的图片都存在这个文件夹中
     ├── train.csv: 对应训练集的标签文件
     ├── val.csv: 对应验证集的标签文件
     |── test.csv: 对应测试集的标签文件
     └── imagenet_class_index.json      图片对应索引关系[索引号: [代码, 名称]]
|—— class_indices.json       图片对应索引关系[代码: [索引号, 名称]]  运行generate_classes_name_json.py生成
```
- train.csv包含38400张图片，共64个类别。
- val.csv包含9600张图片，共16个类别。
- test.csv包含12000张图片，共20个类别

### 2.文件说明
```
核心
|—— generate_classes_name_json.py  调换键值关系
├── test_imagenet_dataset.py      测试生成DataSet和DataLoar的批量效果
├── imagenet_dataset.py     定义ImageNet的DataSet数据格式
├── utils.py                文件夹读取和划分，绘制批量数据
```
首先运行
```python generate_classes_name_json.py``` 调换键值关系 
