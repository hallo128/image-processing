import os
import json

def generate_imagenet_class_name_json(file_json: str,
                                         sava_path= './'):
    '''
    将索引对应文件的键值调换
    file_json: 待调换的json文件 
    sava_path: 保存路径
    '''
    # 读取已有的json文件
    assert os.path.exists(file_json), "file:'{}' not found.".format(file_json)
    class_indices = json.load(open(file_json, "r"))

    # 调换字典的键值
    json_str = json.dumps(dict((val[0], [int(key), val[1]]) for key, val in class_indices.items()), indent=4)
    print(json_str)

    out_file = os.path.join(sava_path, 'classes_name.json')
    with open(out_file, 'w') as json_file:
        json_file.write(json_str)

def main():
    file_json = '/root/.advertorch/mini_imagenet/imagenet_class_index.json'
    generate_imagenet_class_name_json(file_json, sava_path='/home/lighthouse/gitee/image-processing/image_classfication/mini-imagenet')


if __name__ == '__main__':
    main()