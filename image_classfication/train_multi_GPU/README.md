## 1. 文件说明
```
准备文件：
├── resNet34.pth            预训练权重
运行文件：
├── model.py                自定义模型（ResNet模型搭建）【非必须】
├── multi_train_utils       
    ├── distributed_utils.py    多卡训练
    ├── train_eval_utils.py     训练和评估（训练集、验证集）
├── train_simple_gpu.py     单机单卡的训练
├── predict.py              单张图像预测
└── batch_predict.py        批量图像预测
生成文件：
├── weights                 存放每次epoch的模型参数
├── class_indices.json      标签值和索引【自定义数据集会生成，否则需要自己手动添加】
├── class_names.jpg         读入数据的类别条形图
├── predict_result.csv  批量预测的结果数据

```

## 2. 下载预训练好的模型
# resnet34 官方权重下载地址
https://download.pytorch.org/models/resnet34-333f7ec4.pth 【CPU的权重】
https://download.pytorch.org/models/resnet34-b627a593.pth 【GPU的权重】

1. 下载预训练好的权重(放到当前文件夹)【务必根据你的设备对应下载】
2. 重命名
```
wget https://download.pytorch.org/models/resnet34-333f7ec4.pth

mv resnet34-333f7ec4.pth resNet34.pth
```   

## 3. 单GPU启动指令
冻结最后一层，并修改epochs轮数
```python train_single_gpu.py --freeze-layers True --epochs 5 ```


### 3.1 核心步骤
1. 启动wandb监控
2. 数据读取并划分训练集和测试集
3. 数据预处理
    1. 指定图像预处理的transform方法
    2. DataSet
    2. DataLoder
4. 模型model载入
    - 法一：自己写模型model.py
    - 法二：调用已有的模型 
    ```import torchvision.models.resnet as resnet```
5. 载入预训练权重
    1. 下载好对应模型的权重文件.pth
    2. 是否冻结最后一层权重，还是全部都训练
6. 设置超参数：优化器，学习率，学习率调整器
7. 学习（按轮数epoch）
    1. 训练
    2. 更新学习率
    3. 评估
    4. 计算评价指标，并写入监控wandb
    5. 保存本轮的模型参数


### 3.2 wandb监控
每个epoch输出一个监控数据

## 4. 单张图像预测
### 4.1 步骤
1. 加载图片并预处理
  - 单张图片的路径
    - Image.open      读取图片
    - data_transform  图片预处理
    - unsqueeze       增加一个批量维度
2. 创建模型并加载训练好的模型权重
3. 模型预测
4. 输出预测信息
    - 读取标签字典
    - 对应标签输出概率
    - 在原图的基础上增加概率标题

### 4.2 代码修改
1. 准备class_indices.json（标签索引对应文件）【非必须】
2. 修改：
    - img_path：待预测的图片路径
    - weights_path：train训练好的模型参数
3. 输出：
    - 各个类别的概率
    - 在原图基础上增加预测类别和概率

## 5. 批量图像预测
### 5.1 步骤
1. 加载图片并预处理
  - 所有图像路径列表
2. 创建模型并加载训练好的模型权重
3. 模型预测：批量读取图片并预测
  - 批量处理保存为列表
    - Image.open      读取图片
    - data_transform  图片预处理
  - 将img_list列表中的所有图像打包成一个batch
  - batch图像进行预测

### 5.2 代码修改
1. 准备class_indices.json（标签索引对应文件）【非必须】
2. 修改：
    - imgs_root: 指向需要遍历预测的图像文件夹
    - weights_path：train训练好的模型参数
3. 输出：
    - 各个类别的概率(屏幕+"predict_result.csv"文件)

## 6. 多GPU启动指令
- 如果要使用```train_multi_gpu_using_launch.py```脚本，使用以下指令启动
- ```python -m torch.distributed.launch --nproc_per_node=8 --use_env train_multi_gpu_using_launch.py```
- 其中```nproc_per_node```为并行GPU的数量
- 如果要指定使用某几块GPU可使用如下指令，例如使用第1块和第4块GPU进行训练：
- ```CUDA_VISIBLE_DEVICES=0,3 python -m torch.distributed.launch --nproc_per_node=2 --use_env train_multi_gpu_using_launch.py```

-----

- 如果要使用```train_multi_gpu_using_spawn.py```脚本，使用以下指令启动
- ```python train_multi_gpu_using_spawn.py```

## 训练时间对比
![training time](training_time.png)

## 是否使用SyncBatchNorm
![syncbn](syncbn.png)

## 单GPU与多GPU训练曲线
![accuracy](accuracy.png)
