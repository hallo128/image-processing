## 对抗训练


## 依赖环境
```
pip install advertorch
pip install wandb
```

### advertorch 使用

- github网址：https://github.com/BorealisAI/advertorch
- 官方文档：https://advertorch.readthedocs.io/en/latest/
- 攻击方法说明：https://advertorch.readthedocs.io/en/latest/advertorch/attacks.html#attacks

#### 安装advertorch
```pip install advertorch```

#### wandb监控
1. 安装 wandb：```pip install wandb```
2. 登录 wandb：在命令行中运行 ```wandb login```

### 1. 文件说明
```
运行文件：
├── advertorch_examples             advertorch使用测试 
    ├── gener_single_advimg.py      1 单张图片生成对抗样本
    ├── tutorial_train_mnist.py     2 mnist数据集的对抗样本生成并给出标准训练和对抗训练的损失和准确率 
    ├── data                        1 若网络下载存在问题，提前准备的imagenet相关数据
生成文件：
    ├── panda_advimg.jpg            1 gener_single_advimg生成的对抗样本
    ├── trained_models              2 存放训练好的模型参数

```

#### 2. advertorch_examples 测试文件
##### 2.1 单张图片的生成
取自imagenet对应的训练模型，若网络下载存在问题，前提准备下面文件到```/root/.advertorch/data```：
1. panda.jpg        待生成的熊猫图片
2. imagenet_class_index.json

```
# 准备上述文件，已经存放在 advtrain/advertorch_examples/data，拷贝到对应路径

cp panda.jpg /root/.advertorch/data

cp imagenet_class_index.json /root/.advertorch/data

```

运行：```python gener_single_advimg.py```   


##### 2.2 对抗训练

在mnist数据集上, 进行对抗训练得到鲁棒模型

1. 代码运行
- 标准训练：```python tutorial_train_mnist.py```
- 对抗训练：```python tutorial_train_mnist.py --mode "adv"```

2. 结果生成
- ./trained_models/mnist_lenet5_clntrained.pt   标准模型参数
- ./trained_models/mnist_lenet5_advtrained.pt   对抗模型参数
