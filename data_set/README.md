## 主要功能
1. 数据集下载存放[download_flower_data.sh]
2. 划分文件夹：训练集和测试集（非必要）[split_data.py]

## 图像数据集来源
1. url下载
- 图片下载
  - 必应图片下载工具：https://github.com/gurugaurav/bing_image_downloader


- （1）图片存放文件夹
```
├── flower_photos（文件夹）   
       ├── daisy（类别名） 
          ├── 图片1.jpg
          ├── 图片2.jpg
       ├── ...  
       └── roses（类别名） 
          ├── 图片1.jpg
          ├── 图片2.jpg
```
- （2）需要自己定义DataSet 或者 ImageFolder载入
2. pytorch自带数据集

https://pytorch.org/vision/stable/datasets.html

## 该文件夹是用来存放训练数据的目录
### 使用步骤如下：
* （1）在data_set文件夹下创建新文件夹"flower_data"
* （2）点击链接下载花分类数据集 [https://storage.googleapis.com/download.tensorflow.org/example_images/flower_photos.tgz](https://storage.googleapis.com/download.tensorflow.org/example_images/flower_photos.tgz)
* （3）新建flower_data文件夹，解压数据集到flower_data文件夹下
* （4）执行"split_data.py"脚本自动将数据集划分成训练集train和验证集val  
  -  split_data.py：在下载不同数据集的时候，需要新建文件夹和更改文件夹名称

### 终端运行（当前路径下）：
download_flower_data.sh：数据集下载并新建文件夹，解压代码
```
chmod +x download_flower_data.sh
./download_flower_data.sh
python split_data.py
```

### 最终划分的数据集：
```
├── flower_data   
       ├── flower_photos（解压的原数据集文件夹，3670个样本）  
       ├── train（生成的训练集，3306个样本）  
       └── val（生成的验证集，364个样本） 
```

### mini-imagenet数据集
- 步骤
  - 1.数据下载
    - mini-imagenet目录
    - imagenet_class_index.json【类别对应索引】
  - 2.从数据集合中，划分数据到文件夹中

- 文件
  - 1.split_mini_imagenet.py 数据划分程序

#### 1.数据目录

- train.csv包含38400张图片，共64个类别。
- val.csv包含9600张图片，共16个类别。
- test.csv包含12000张图片，共20个类别
```
├── mini-imagenet: 数据集根目录
     ├── images: 所有的图片都存在这个文件夹中
     ├── train.csv: 对应训练集的标签文件
     ├── val.csv: 对应验证集的标签文件
     └── test.csv: 对应测试集的标签文件
```

#### 2.划分程序
```
python split_mini_imagenet.py
```


参考：https://blog.csdn.net/qq_37541097/article/details/113027489?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522170969682516800215015128%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fblog.%2522%257D&request_id=170969682516800215015128&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~blog~first_rank_ecpm_v1~rank_v31_ecpm-8-113027489-null-null.nonecase&utm_term=%E6%95%B0%E6%8D%AE&spm=1018.2226.3001.4450



### 必应图片下载工具
https://github.com/gurugaurav/bing_image_downloader

安装下载工具
```
pip install bing-image-downloader
```
必应图片下载python代码
```
from bing_image_downloader import downloader
downloader.download("芒果",       #要搜索的图片名
                    #数量
                    limit=100,    
                    #输出路径，默认当前文件夹下的dataset文件夹内
                    output_dir='dataset',    
                    
                    adult_filter_off=True, 
                    #如果文件夹已经存在是否删除
                    force_replace=False,  
                    #60秒超时
                    timeout=60,    
                    #filter="jpg", 
                    verbose=True)     #输出下载的信息

#过滤的图片类型
"jpe", "jpeg", "jfif", "exif", "tiff", "gif", "bmp", "png", "webp", "jpg"
```

